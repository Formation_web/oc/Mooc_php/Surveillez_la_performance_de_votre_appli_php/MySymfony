<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        if ($this->has('debug.stopwatch')) {
            $stopwatch = $this->get('debug.stopwatch');

            $stopwatch->start('sleeps');
            for ($i = 1; $i < 10; $i++) {
                sleep(rand(1, 3));
                $stopwatch->lap('sleeps');
            }

            $stopwatch->stop('sleeps');
        }
        
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }
}
